package com.epam.training.sportsbetting.repository;

import com.epam.training.sportsbetting.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
