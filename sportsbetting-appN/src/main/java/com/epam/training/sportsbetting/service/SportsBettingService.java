package com.epam.training.sportsbetting.service;

import java.math.BigDecimal;
import java.util.List;

import com.epam.training.sportsbetting.domain.Bet;
import com.epam.training.sportsbetting.domain.Outcome;
import com.epam.training.sportsbetting.domain.OutcomeOdd;
import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.domain.SportEvent;
import com.epam.training.sportsbetting.domain.Wager;
import javassist.NotFoundException;

public interface SportsBettingService {	
	BigDecimal calculateResults(BigDecimal act, BigDecimal bet, BigDecimal multiply);
	BigDecimal calculateResults(BigDecimal act, BigDecimal bet);
	Player findPlayer(Long id) throws NotFoundException;
	List<SportEvent> findAllSporEvents();
	List<Wager> findAllWagers(List<Wager> wager);
	List<Bet> findAllBets();
	List<Outcome> findAllOutCome();
	List<OutcomeOdd> findAllOutComeOdd();
	void savePlayer(Player player);
	void saveWager(Wager wager);
	void saveBet(Bet bet);
	void saveOutcome(Outcome outcome);
	void saveOutcomeOdd(OutcomeOdd outcomeOdd);
	void saveSportEvent(SportEvent sportEvent);	
	void createPlayer();
	void createAll();
	/*void createOutcomeOdd();
	void createOutcome();
	void createBet();
	void createSportEvent();
	void createEvent();*/
	
	
		
}

