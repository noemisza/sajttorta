package com.epam.training.sportsbetting.repository;

import com.epam.training.sportsbetting.domain.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {
}
