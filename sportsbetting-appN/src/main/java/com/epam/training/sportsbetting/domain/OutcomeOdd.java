package com.epam.training.sportsbetting.domain;

import lombok.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OutcomeOdd {
	@Id
	@GeneratedValue
	private Long id;
	private BigDecimal value;
	private LocalDateTime validFrom;
	private LocalDateTime validUntil;

	@Enumerated
	private Currency currency;

	@OneToMany(mappedBy = "outcomeOdd", cascade = CascadeType.PERSIST)
	@Singular
	@ElementCollection
	private List<Wager> wagers;

	@ManyToOne
	private Outcome outcome;
}
