package com.epam.training.sportsbetting.repository;

import com.epam.training.sportsbetting.domain.Wager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WagerRepository extends JpaRepository<Wager, Long> {
}
