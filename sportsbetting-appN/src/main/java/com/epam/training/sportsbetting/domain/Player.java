package com.epam.training.sportsbetting.domain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Player {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private Integer accountNumber;
	private BigDecimal balance;
	private LocalDate birth;

	@Enumerated
	private Currency currency;

	@OneToOne
	private Wager wager;
}

