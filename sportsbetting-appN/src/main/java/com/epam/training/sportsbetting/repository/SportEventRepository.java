package com.epam.training.sportsbetting.repository;

import com.epam.training.sportsbetting.domain.SportEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SportEventRepository extends JpaRepository<SportEvent, Long> {
}
