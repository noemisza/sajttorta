package com.epam.training.sportsbetting;
import com.epam.training.sportsbetting.domain.Currency;
import com.epam.training.sportsbetting.domain.Wager;
import com.epam.training.sportsbetting.service.ServiceMeth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.view.ViewMeth;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Random;

@SpringBootApplication
public class SportsbettingAppApplication {
	
	@Autowired
	private ViewMeth viewMeth;

	@Autowired
	private ServiceMeth serviceMeth;
		
	public static void main(String[] args) throws IOException {
		ConfigurableApplicationContext context =
			SpringApplication.run(SportsbettingAppApplication.class, args);
		SportsbettingAppApplication app =
				context.getBean(SportsbettingAppApplication.class);
		app.play();
	}
	public void play() throws IOException {
		serviceMeth.createPlayer();
		serviceMeth.createAll();				
		
		boolean isProcessed = false;
		boolean isWin = false;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		BigDecimal OddOne = new BigDecimal(2); 
		BigDecimal OddTwo = new BigDecimal(3);
		int wagerOutcomeOdd = 0;
				
		System.out.println("What is your name?");		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));		
		String name = reader.readLine();
		
		System.out.println("What is your currency? (HUF, EUR or USD)");
		reader = new BufferedReader(new InputStreamReader(System.in));		
		String currency = reader.readLine().toUpperCase();		
		
		System.out.println("How much money do u have?");
		InputStreamReader str = new InputStreamReader(System.in);
		BufferedReader BigDReader = new BufferedReader(str); 				
		String BigD = BigDReader.readLine();
		BigDecimal balance = new BigDecimal(BigD);

		Player player = Player.builder()
			.name(name)
			.currency(Currency.of(currency))
			.balance(balance)
			.build();
		serviceMeth.savePlayer(player);

		viewMeth.printWelcomeMessage(player);
		
		while(player.getBalance() != null && !reader.readLine().equals("q")){			
			System.out.println("What do you want to bet on? (choose a number or press q to quit)");									
			serviceMeth.findAllBets().forEach(System.out::println);			
			reader = new BufferedReader(new InputStreamReader(System.in));			
			Long choosen = Long.parseLong(reader.readLine());
			
			System.out.println("What amount do you wish to bet on it?");			
			reader = new BufferedReader(new InputStreamReader(System.in));
			BigDecimal amount = BigDecimal.valueOf(Long.parseLong(reader.readLine()));
		while(player.getBalance().subtract(amount).compareTo(BigDecimal.ZERO) < 0){
			System.out.println(player + " You don't have enough balance");
			reader = new BufferedReader(new InputStreamReader(System.in));
			amount = BigDecimal.valueOf(Long.parseLong(reader.readLine()));
		}
		
		System.out.println(player.getName() + " your bet is saved");
		isProcessed = true;		
		
		Wager wager = Wager.builder()
				.player(player)
				.amount(amount)				
				.processed(isProcessed)
				.timestampCreated(timestamp)				
				.currency(Currency.of(currency))
				.build();
		serviceMeth.saveWager(wager);
		
		
		Random rnd = new Random();
		int randN = rnd.nextInt(4) + 1;		
		
		if (randN == choosen) {								
			player.setBalance(serviceMeth.calculateResults(player.getBalance(), amount,  OddOne));
			isWin = true;
			//wagerOutcomeOdd = 2;
			viewMeth.printBalance(player);			
		}else if (randN == choosen) {								
			player.setBalance(serviceMeth.calculateResults(player.getBalance(), amount, OddTwo));				
			viewMeth.printBalance(player);
			//wagerOutcomeOdd = 3;
			isWin = true;
		}else if (randN == choosen) {								
			player.setBalance(serviceMeth.calculateResults(player.getBalance(), amount, OddOne));				
			viewMeth.printBalance(player);
			//wagerOutcomeOdd = 2;
			isWin = true;
		}else if (randN == choosen) {								
			player.setBalance(serviceMeth.calculateResults(player.getBalance(), amount, OddTwo));				
			viewMeth.printBalance(player);
			//wagerOutcomeOdd = 3;
			isWin = true;
		}else {
			player.setBalance(serviceMeth.calculateResults(player.getBalance(), amount));
			viewMeth.printBalance(player);
		}
		
		wager.builder()
			.hasWon(isWin)
			//.outcomeOdd(wagerOutcomeOdd)
			.build();
		serviceMeth.saveWager(wager);
		/* printresult még nincs megírva
		viewMeth.printResults(player, wager);
		*/
		
		}
		
		
		
	}
}
	

