package com.epam.training.sportsbetting.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;
import org.dom4j.IllegalAddException;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Outcome {

	@Id
	@GeneratedValue
	private Long id;

	private String description;

	@ManyToOne
	private Bet bet;

	@ElementCollection
	@Singular
	@OneToMany(mappedBy = "outcome", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<OutcomeOdd> outcomeOdds;

	@ManyToOne
	private Result result;

	public void addOutcomeOdd(OutcomeOdd outcomeOdd){
		LocalDateTime validFrom = outcomeOdd.getValidFrom();
		LocalDateTime validTo = outcomeOdd.getValidUntil();

		for (OutcomeOdd current : outcomeOdds){
			if (current.getValidFrom().isBefore(validFrom) && current.getValidUntil().isAfter(validFrom)){
				throw new IllegalAddException("Can not add odd because there is already one in that timerange");
			} else if(current.getValidFrom().isBefore(validTo) && current.getValidUntil().isAfter(validTo)){
				throw new IllegalAddException("Can not add odd because there is already one in that timerange");
			}
		}

		outcomeOdds.add(outcomeOdd);
	}
}

