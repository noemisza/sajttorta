package com.epam.training.sportsbetting.domain;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SportEvent {
	@Id
	@GeneratedValue
	private Long id;
	private String title;
	private LocalDateTime startDate;
	private LocalDateTime endDate;

	@ElementCollection
	@OneToMany(mappedBy = "sportEvent" , cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	//@Singular
	//private List<Bet> bets;

	@OneToOne
	private Result result;
}
