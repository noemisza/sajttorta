package com.epam.training.sportsbetting.view;

import java.math.BigDecimal;
import java.util.List;
import com.epam.training.sportsbetting.domain.OutcomeOdd;
import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.domain.SportEvent;
import com.epam.training.sportsbetting.domain.Wager;
import org.springframework.stereotype.Service;

@Service
public class ViewMeth implements View{

	@Override
	public Player readPlayerData() {
		return null;
	}

	@Override
	public BigDecimal readWagerAmount() {
		return null;
	}
	
	@Override
	public OutcomeOdd printOutcomeOdd(List<SportEvent> sportEvents) {
		System.out.printf("");
		return null;
	}

	@Override
	public void printWelcomeMessage(Player player) {
		System.out.println("Welcome " + player.getName());
		printBalance(player);
	}

	@Override
	public void printBalance(Player player) {
		System.out.println("Your balance is: " + player.getBalance() + " " + player.getCurrency());
	}

	@Override //ezt kéne tudni hogy mit csinál 
	public void printOutcomeOdds(List<SportEvent> sportEvents) {
		/*sportEvents.forEach(sportEvent ->{
			sportEvent.getBets().forEach(bet -> {
				bet.getOutcomes().forEach(outcome -> {
					outcome.getOutcomeOdds().forEach(System.out::println);
				});
			});
		});
		 */		
	}

	@Override
	public void printResults(Player player, List<Wager> wager) {
		System.out.printf("");
		
	}	
}
	
