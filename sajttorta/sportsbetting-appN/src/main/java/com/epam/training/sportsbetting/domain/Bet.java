package com.epam.training.sportsbetting.domain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Bet {
	@Id
	@GeneratedValue
	private Long id;
	private String description;

	@Enumerated
	private BetType betType;

	@ManyToOne
	private SportEvent sportEvent;

	/*
	@ElementCollection
	@Singular
	@OneToMany
	private List<Outcome> outcomes;
	*/
}

