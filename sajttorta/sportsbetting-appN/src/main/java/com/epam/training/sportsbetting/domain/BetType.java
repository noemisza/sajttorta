package com.epam.training.sportsbetting.domain;


public enum BetType {
		WINNER,
		GOALS,
		PLAYERS_SCORE,
		NUMBERS_OF_SETS;

	public static BetType of(String betType){
		BetType result = BetType.GOALS;

		switch(betType){
			case "WINNER":
				result = BetType.WINNER;
				break;
			case "GOALS":
				result = BetType.GOALS;
				break;
			case "PLAYERS_SCORE":
				result = BetType.PLAYERS_SCORE;
				break;
			case "NUMBERS_OF_SETS":
				result = BetType.NUMBERS_OF_SETS;
				break;
		}

		return result;
	}

	BetType() {
	}
}

