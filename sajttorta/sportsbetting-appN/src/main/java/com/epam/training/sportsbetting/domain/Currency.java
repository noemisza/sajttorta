package com.epam.training.sportsbetting.domain;

public enum Currency {
	HUF,
	EUR,
	USD;

	public static Currency of(String currency){
		Currency result = Currency.HUF;

		switch(currency){
			case "HUF":
				result = Currency.HUF;
				break;
			case "EUR":
				result = Currency.EUR;
				break;
			case "USD":
				result = Currency.USD;
				break;
		}

		return result;
	}
}

