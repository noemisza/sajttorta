package com.epam.training.sportsbetting.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    @Id
    @GeneratedValue
    private Long id;

    @ElementCollection
    @Singular
    @OneToMany(mappedBy = "result", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Outcome> possibleOutcomes;

    @OneToOne
    private SportEvent sportEvent;
}
