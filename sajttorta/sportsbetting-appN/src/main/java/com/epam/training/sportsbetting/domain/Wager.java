package com.epam.training.sportsbetting.domain;

import lombok.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Wager {
	@Id
	@GeneratedValue
	private Long id;

	private BigDecimal amount;
	private Timestamp timestampCreated;
	private boolean processed;
	private boolean hasWon;

	@ManyToOne
	private OutcomeOdd outcomeOdd;

	@Enumerated
	private Currency currency;

	@OneToOne
	private Player player;
}

