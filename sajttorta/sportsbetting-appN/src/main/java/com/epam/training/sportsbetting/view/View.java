package com.epam.training.sportsbetting.view;
import java.math.BigDecimal;
import java.util.List;
import com.epam.training.sportsbetting.domain.OutcomeOdd;
import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.domain.SportEvent;
import com.epam.training.sportsbetting.domain.Wager;

public interface View {
	Player readPlayerData();
	BigDecimal readWagerAmount();
	OutcomeOdd printOutcomeOdd(List<SportEvent> sportEvents);
	void printWelcomeMessage(Player player);
	void printBalance(Player player);
	void printOutcomeOdds(List<SportEvent> sportEvents);
	void printResults(Player player, List<Wager> wager);
}

