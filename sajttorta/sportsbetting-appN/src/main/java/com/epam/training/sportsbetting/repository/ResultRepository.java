package com.epam.training.sportsbetting.repository;

import com.epam.training.sportsbetting.domain.Result;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResultRepository extends JpaRepository<Result, Long> {
}
