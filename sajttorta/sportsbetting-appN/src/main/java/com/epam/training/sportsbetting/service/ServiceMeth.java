package com.epam.training.sportsbetting.service;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import com.epam.training.sportsbetting.domain.Bet;
import com.epam.training.sportsbetting.domain.BetType;
import com.epam.training.sportsbetting.domain.Currency;
import com.epam.training.sportsbetting.domain.Outcome;
import com.epam.training.sportsbetting.domain.OutcomeOdd;
import com.epam.training.sportsbetting.domain.Player;
import com.epam.training.sportsbetting.domain.SportEvent;
import com.epam.training.sportsbetting.domain.Wager;
import com.epam.training.sportsbetting.repository.BetRepository;
import com.epam.training.sportsbetting.repository.OutcomeOddRepository;
import com.epam.training.sportsbetting.repository.OutcomeRepository;
import com.epam.training.sportsbetting.repository.PlayerRepository;
import com.epam.training.sportsbetting.repository.SportEventRepository;
import com.epam.training.sportsbetting.repository.WagerRepository;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ServiceMeth implements SportsBettingService{

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private SportEventRepository sportEventRepository;

	@Autowired
	private BetRepository betRepository;

	@Autowired
	private WagerRepository wagerRepository;
	
	@Autowired
	private OutcomeRepository outcomeRepository;

	@Autowired
	private OutcomeOddRepository outcomeOddRepository;

	@Override
	public Player findPlayer(Long id) throws NotFoundException {
		log.info("Finding player with id: " + id);
		return playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Player not found"));
	}

	@Override
	public List<SportEvent> findAllSporEvents() {
		log.info("Finding all sportEvents");
		return sportEventRepository.findAll();
	}
	
	@Override
	public List<Bet> findAllBets() {
		log.info("Finding all sportEvents");
		return betRepository.findAll();
	}	
	
	@Override
	public List<Outcome> findAllOutCome() {
		log.info("Finding all sportEvents");
		return outcomeRepository.findAll();
	}
	
	@Override
	public List<OutcomeOdd> findAllOutComeOdd() {
		log.info("Finding all sportEvents");
		return outcomeOddRepository.findAll();
	}
	
	@Override
	public List<Wager> findAllWagers(List<Wager> wager) {
		log.info("Finding all wagers");
		return wagerRepository.findAll();
	}
	
	@Override
	public void savePlayer(Player player) {
		log.info("Saving player with name: " + player.getName());
		playerRepository.save(player);
	}
	
	@Override
	public void saveSportEvent(SportEvent sportEvent) {
		sportEventRepository.save(sportEvent);
	}
	
	@Override
	public void saveBet(Bet bet) {
		betRepository.save(bet);
	}	
	
	@Override
	public void saveOutcomeOdd(OutcomeOdd outcomeOdd) {
		outcomeOddRepository.save(outcomeOdd);
	}
	@Override
	public void saveWager(Wager wager) {
		log.info("Saving wager from player: " + wager.getPlayer());
		wagerRepository.save(wager);
	}
	
	@Override
	public void saveOutcome(Outcome outcome) {
		outcomeRepository.save(outcome);
	}
	
	@Override
	public BigDecimal calculateResults(BigDecimal act, BigDecimal bet, BigDecimal multiply) {		
		return bet.multiply(multiply).add(act);
	}
	
	@Override
	public BigDecimal calculateResults(BigDecimal act, BigDecimal bet) {		
		return act.subtract(bet);
	}	

	@Override
	public void createPlayer() {
		BigDecimal balance = new BigDecimal(500);
		Player player = Player.builder()
			.name("Kis János")
			.currency(Currency.of("USD"))
			.balance(balance)
			.build();
		savePlayer(player);
	}
	
	@Override
	public void createAll() {		
//SportEvent create
		LocalDateTime startDate = LocalDateTime.now().minusHours(2);
		LocalDateTime endDate = LocalDateTime.now().plusHours(2);
		SportEvent sportEvent = SportEvent.builder()
			.title("Felcsút vs Arsenal")
			.startDate(startDate)
			.endDate(endDate)				
			.build();
		saveSportEvent(sportEvent);		
//Bet create
		Bet bet1 = Bet.builder()
			.description("Number of goals")
			.betType(BetType.of("GOALS"))
			.sportEvent(sportEvent)
			.build();		
		saveBet(bet1);
		
		Bet bet2 = Bet.builder()
			.description("Number of goals")
			.betType(BetType.of("GOALS"))
			.sportEvent(sportEvent)				
			.build();
		saveBet(bet2);	
		Bet bet3 = Bet.builder()
			.description("Number of goals")
			.betType(BetType.of("GOALS"))
			.sportEvent(sportEvent)
			.build();
		saveBet(bet3);	
		Bet bet4 = Bet.builder()
			.description("The winner team")
			.betType(BetType.of("WINNER"))
			.sportEvent(sportEvent)
			.build();
		saveBet(bet4);	
		
//Outcome create	
		Outcome outcome1 = Outcome.builder()
			.description("1")
			.bet(bet1)
			.build();
		saveOutcome(outcome1);
		
		Outcome outcome2 = Outcome.builder()
			.description("0")
			.bet(bet1)
			.build();
		saveOutcome(outcome2);
		
		Outcome outcome3 = Outcome.builder()
			.description("2")
			.bet(bet1)
			.build();
		saveOutcome(outcome3);
		
		Outcome outcome4 = Outcome.builder()
			.description("Win")
			.bet(bet2)			
			.build();
		saveOutcome(outcome4);
		
//Outcome odd create
		LocalDateTime date1 = LocalDateTime.now().minusMinutes(90);
		LocalDateTime date2 = LocalDateTime.now().plusMinutes(10);
		LocalDateTime date3= LocalDateTime.now().plusMinutes(11);
		LocalDateTime date4= LocalDateTime.now().plusMinutes(91);
		BigDecimal bg = new BigDecimal(2);
		BigDecimal bg2 = new BigDecimal(3);
		
		OutcomeOdd outcomeOdd1 = OutcomeOdd.builder()
			.validFrom(date1)
			.validUntil(date2)
			.value(bg)
			.outcome(outcome1)	
			.currency(Currency.of("HUF"))
			.build();
		saveOutcomeOdd(outcomeOdd1);
		
		OutcomeOdd outcomeOdd2 = OutcomeOdd.builder()
			.validFrom(date1)
			.validUntil(date2)
			.value(bg2)
			.outcome(outcome2)
			.currency(Currency.of("HUF"))
			.build();
		saveOutcomeOdd(outcomeOdd2);
		
		OutcomeOdd outcomeOdd3 = OutcomeOdd.builder()
			.validFrom(date1)
			.validUntil(date2)
			.value(bg)
			.outcome(outcome3)
			.currency(Currency.of("HUF"))
			.build();
		saveOutcomeOdd(outcomeOdd3);
		
		OutcomeOdd outcomeOdd4 = OutcomeOdd.builder()
			.validFrom(date3)
			.validUntil(date4)
			.value(bg2)
			.outcome(outcome4)
			.currency(Currency.of("HUF"))
			.build();
		saveOutcomeOdd(outcomeOdd4);

	}
	
	
}




/*
@Override
public void createSportEvent() {
	LocalDateTime startDate = LocalDateTime.now().minusHours(2);
	LocalDateTime endDate = LocalDateTime.now().plusHours(2);
	SportEvent sportEvent = SportEvent.builder()
			.title("Felcsút vs Arsenal")
			.startDate(startDate)
			.endDate(endDate)
			//.bet(null)
			.build();
	saveSportEvent(sportEvent);
}

@Override
public void createEvent() {
	// TODO Auto-generated method stub
	
}

@Override
public void createBet() {
	Bet bet = Bet.builder()
			.description("Gólok száma")
			.betType(BetType.of("GOALS"))
			.build();
	saveBet(bet);
	/*
	LocalDateTime startDate = LocalDateTime.now().minusHours(2);
	LocalDateTime endDate = LocalDateTime.now().plusHours(2);

		SportEvent sportEvent2  = SportEvent.builder()
			.title("Felcsút vs Arsenal")
			.startDate(startDate)
			.endDate(endDate)
			.build();
saveSportEvent(sportEvent2);
*
	Bet bet2 = Bet.builder()
			.description("Gólok száma")
			.betType(BetType.of("GOALS"))
//			.sportEvent(sportEvent2)
			.build();
	saveBet(bet2);
	}


@Override
public void createOutcome() {
	Outcome outcome = Outcome.builder()
			.description("1")
//			.bet(bet)
			.build();
	saveOutcome(outcome);
	Outcome outcome1 = Outcome.builder()
			.description("0")
//			.bet(bet)
			.build();
	saveOutcome(outcome1);
	Outcome outcome2 = Outcome.builder()
			.description("1")
//			.bet(bet)
			.build();
	saveOutcome(outcome2);

}


@Override
public void createOutcomeOdd() {
	// TODO Auto-generated method stub
	
}
*/
