package com.epam.training.sportsbetting.domain;

import org.dom4j.IllegalAddException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class OutcomeTest {

    private Outcome outcome;

    @Before
    public void beforeMethod(){
        outcome = new Outcome();
        List<OutcomeOdd> outcomeOdds = new ArrayList<>();
        OutcomeOdd example = OutcomeOdd.builder()
                    .validFrom(LocalDateTime.now())
                    .validUntil(LocalDateTime.now().plusHours(2))
                    .build();
        outcomeOdds.add(example);
        outcome.setOutcomeOdds(outcomeOdds);
    }

    @After
    public void tearDown(){
        outcome = null;
    }

    @Test
    public void addOutcomeOdd() throws Exception {
        OutcomeOdd example = OutcomeOdd.builder()
                .validFrom(LocalDateTime.now().plusHours(3))
                .validUntil(LocalDateTime.now().plusHours(4))
                .build();
        outcome.addOutcomeOdd(example);

        assertTrue(outcome.getOutcomeOdds().contains(example));
    }

    @Test(expected = IllegalAddException.class)
    public void couldNotAddOutcomeOdd() throws Exception {
        OutcomeOdd example = OutcomeOdd.builder()
                .validFrom(LocalDateTime.now().plusHours(1))
                .validUntil(LocalDateTime.now().plusHours(1).plusMinutes(15))
                .build();
        outcome.addOutcomeOdd(example);
    }
}
